import express, { Express, Request, Response } from "express";
import dotenv from "dotenv";

dotenv.config();

const app: Express = express();

app.get("/", (req: Request, res: Response) => {
  res.send("Hello World!");
});

app.get("/hi", (req: Request, res: Response) => {
    res.send("Hi!");
})

const port: string | number = process.env.PORT || "3000";

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
